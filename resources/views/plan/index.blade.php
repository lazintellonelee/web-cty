@extends('layout.user')

@section('title', 'Plan')

@section('breadcrumb')
<li><a href="<?= route('home')?>">Home</a></li>
<li><a href="javascript::void()">Plan</a></li>
@endsection

@section('headstyle')
<link href='<?= asset("public/plugins/fullcalendar/fullcalendar.css")?>' rel='stylesheet' />
<link href='<?= asset("public/plugins/fullcalendar/fullcalendar.print.css")?>' rel='stylesheet' media='print' />
@endsection

@section('inlinescript')
<script src='<?= asset("public/plugins/fullcalendar/lib/moment.min.js")?>'></script>
<script src='<?= asset("public/plugins/fullcalendar/fullcalendar.min.js")?>'></script>
<script src='<?= asset("public/js/plan/functions.js")?>'></script>
<script src='<?= asset("public/js/plan/index.js")?>'></script>
@endsection

@section('content')
<div class="modal fade" id="modal-add-event">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Event</h4>
            </div>
            <div class="modal-body row">
                <div class="col-sm-12 col-xs-12 margin-5"><b>Event Name</b></div>
                <div class="col-sm-12 col-xs-12 margin-5">
                    <input class="form-control" id="add-event-name" />
                </div>
                <div class="col-sm-12 col-xs-12 margin-5"><b>Event Description</b></div>
                <div class="col-sm-12 col-xs-12 margin-5">
                    <textarea class="form-control" id="add-event-description" rows="4"></textarea>
                </div>
                <div class="col-sm-12 col-xs-12 margin-5"><b>Event End To</b></div>
                <div class="col-sm-3 col-xs-12 margin-5">
                    <input class="form-control" id="add-event-end" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-fw fa-power-off"></i> Cancel</button>
                <button id="btn-add-event" type="button" class="btn btn-success btn-sm"><i class="fa fa-fw fa-check"></i> Add</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal-edit-event">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Event</h4>
            </div>
            <div class="modal-body row">
                <div class="col-sm-12 col-xs-12 margin-5"><b>Event Name</b></div>
                <div class="col-sm-12 col-xs-12 margin-5">
                    <input class="form-control" id="edit-event-name" />
                </div>
                <div class="col-sm-12 col-xs-12 margin-5"><b>Event Description</b></div>
                <div class="col-sm-12 col-xs-12 margin-5">
                    <textarea class="form-control" id="edit-event-description" rows="4"></textarea>
                </div>
                <div class="col-sm-12 col-xs-12 margin-5"><b>Event End To</b></div>
                <div class="col-sm-3 col-xs-12 margin-5">
                    <input class="form-control" id="edit-event-end" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm pull-left btn-delete-event"><i class="fa fa-fw fa-trash"></i> Delete</button>
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-fw fa-power-off"></i> Cancel</button>
                <button id="btn-edit-event" type="button" class="btn btn-success btn-sm"><i class="fa fa-fw fa-check"></i> Change</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal-detail-event">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="event-name"></h4>
            </div>
            <div class="modal-body row">
                <div class="col-sm-12 col-xs-12 margin-5" id="event-description"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm pull-left btn-delete-event"><i class="fa fa-fw fa-trash"></i> Delete</button>
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-fw fa-power-off"></i> Cancel</button>
                <button id="btn-call-edit-event" type="button" class="btn btn-success btn-sm"><i class="fa fa-fw fa-edit"></i> Edit</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-xs-12">
            <div id="add-new-event">
                <h4 class="page-header">Add new event</h4>
                <div class="form-group">
                    <label>Event title</label>
                    <input type="text" id="new-event-title" class="form-control">
                </div>
                <div class="form-group">
                    <label>Event description</label>
                    <textarea class="form-control" id="new-event-desc" rows="3"></textarea>
                </div>
                <a href="#" id="new-event-add" class="btn btn-primary pull-right">Add event</a>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-9 col-xs-12" id="plan"></div>
    </div>
</div>
@endsection