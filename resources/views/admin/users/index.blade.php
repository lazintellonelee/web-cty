@extends('layout.admin')

@section('title', 'Admin - Users')

@section('inlinescript')
<script src="<?= asset('public/plugins/bootstrap-toggle-master/js/bootstrap-toggle.js')?>"></script>
<script src="<?= asset('public/plugins/datatables/jquery.dataTables.js')?>" type="text/javascript"></script>
<script src="<?= asset('public/plugins/datatables/ZeroClipboard.js')?>" type="text/javascript"></script>
<script src="<?= asset('public/plugins/datatables/TableTools.js')?>" type="text/javascript"></script>
<script src="<?= asset('public/plugins/datatables/dataTables.bootstrap.js')?>" type="text/javascript"></script>
<script src="<?= asset('public/plugins/javascript-template/tmpl.min.js')?>" type="text/javascript"></script>
<script src="<?= asset('public/js/admin/users/index/script.js')?>" type="text/javascript"></script>
@endsection

@section('headstyle')
<link href="<?= asset('public/plugins/bootstrap-toggle-master/css/bootstrap-toggle.css')?>" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<li><a href="<?= route('admin-dashboard')?>">Admin</a></li>
<li><a href="javascript::void()">Users</a></li>
@endsection

@section('content')
<script type="type/x-tmpl" id="tools">
    <div class="col-sm-12 col-xs-12" style="z-index: 100000; margin-top:10px; line-height: 22px">
        <div class="checkbox-inline">
            <label class="text-center">
                <input type="checkbox" id="check-all-users">&nbsp;
                <i class="fa fa-square-o small"></i>
            </label>
        </div>
        &nbsp;<b>Check All</b>
        &nbsp;&nbsp;&nbsp;
        <i>Selected: </i>
        <a href="" id="delete-check-users"><i class="fa fa-fw fa-trash"></i> Delete</a>
        &nbsp;&nbsp;&nbsp;
        <a href="" id="recycle-users"><i class="fa fa-fw fa-recycle"></i> Recycle Users</a>
    </div>
</script>

<script type="type/x-tmpl" id="edit">
    <div class="row">
        <form id="form-edit">
            <input type="hidden" name="user_id" value="{%=o.user_id%}" />
            <div class="col-sm-2 col-xs-2">
                <img class="img-responsive" src="{%=o.user_avatar===''?'<?= asset('public/img/noavatar.gif')?>':'<?= url()?>/public/upload/images/avatar/' + o.user_avatar%}" />
            </div>
            <div class="col-sm-10 col-xs-10 no-padding">
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-2"><b>Username</b></div>
                    <div class="col-sm-5 col-xs-5"><i>{%=o.user_name%}</i></div>
                </div>
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-2 div-line"><b>Firstname</b></div>
                    <div class="col-sm-5 col-xs-5">
                        <input type="text" name="user_firstname" id="user_firstname" class="form-control" value="{%=o.user_firstname%}" />
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-2 div-line"><b>Lastname</b></div>
                    <div class="col-sm-5 col-xs-5">
                        <input type="text" name="user_lastname" id="user_lastname" class="form-control" value="{%=o.user_lastname%}" />
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-2 div-line"><b>Email</b></div>
                    <div class="col-sm-5 col-xs-5">
                        <input type="text" name="user_email" id="user_email" class="form-control" value="{%=o.user_email%}" />
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-2 div-line"><b>Sex</b></div>
                    <div class="col-sm-2 col-xs-2">
                        <select class="form-control" name="user_sex" id="user_sex">
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>
                    <div class="col-sm-1 col-xs-1 div-line"><b>Birthday</b></div>
                    <div class="col-sm-2 col-xs-2">
                        <input type="text" class="form-control" name="user_birthday" id="user_birthday" placeholder="dd/mm/yyyy" value="{%=FormatDateTime(o.user_birthday, 'd/m/Y')%}" />
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-2 div-line"><b>Phone</b></div>
                    <div class="col-sm-5 col-xs-5">
                        <input type="text" name="user_phone" id="user_phone" class="form-control" value="{%=o.user_phone%}" />
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-2 div-line"><b>Address</b></div>
                    <div class="col-sm-5 col-xs-5">
                        <textarea type="text" name="user_address" id="user_address" class="form-control">{%=o.user_address%}</textarea>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-2 div-line"><b>Updated At</b></div>
                    <div class="col-sm-5 col-xs-5">
                        <i>{%=FormatDateTime(o.updated_at, 'H:i d/m/Y')%}</i>
                    </div>
                </div>
        
                <div class="col-sm-7 col-xs-7 margin-5 text-right">
                    <button id="cancel-edit" type="button" class="btn btn-sm btn-danger"><i class="fa fa-power-off"></i>&nbsp;&nbsp;&nbsp;Cancel</button>
                    <button id="btn-edit" type="button" class="btn btn-sm btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;&nbsp;Update</button>
                </div>
            </div>
        </form>
    </div>
</script>

<script type="text/x-tmpl" id="tr-edit">
    <td>
        <center>
            <div class="checkbox-inline">
                <label class="text-center">
                    <input type="checkbox" class="check-user" user-id="{%=o.user_id%}">&nbsp;
                    <i class="fa fa-square-o small"></i>
                </label>
            </div>
        </center>
    </td>
    <td>
        <a href="" class="edit-user" user-id="{%=o.user_id%}">
            <i class="fa fa-fw fa-pencil-square-o"></i> <i>Edit</i>
        </a>
        &nbsp;&nbsp;
        <a href="" class=" delete-user" user-id="{%=o.user_id%}">
            <i class="fa fa-fw fa-trash-o"></i> <i>Delete</i>
        </a>
    </td>
    <td>{%=o.user_firstname + " " + o.user_lastname%}</td>
    <td>{%=o.user_email%}</td>
    <td><input type="checkbox" class="toggle-user-status" user-id="{%=o.user_id%}" {%=o.user_status=='1'?'checked':''%} /></td>
    <td>{%=FormatDateTime(o.created_at, 'H:i d/m/Y')%}</td>
</script>

<script type="text/x-tmpl" id="delete">
    <div class="row">
    {% for(var i=0; i<o.length; i++) { %}
        <div class="col-sm-11 col-xs-11 margin-5">
            <a href="" class="remove-delete-user" user-id="{%=o[i].user_id%}"><i class="fa fa-fw fa-minus-circle" style="color: #D2322D"></i></a>
            <i class="fa fa-fw fa-user"></i> {%=o[i].user_firstname%}&nbsp;{%=o[i].user_lastname%}
        </div>
    {% } %}
        <div class="col-sm-12 col-xs-12 margin-5 text-right">
            <button id="cancel-delete" type="button" class="btn btn-sm btn-danger"><i class="fa fa-power-off"></i>&nbsp;&nbsp;&nbsp;Cancel</button>
            <button id="btn-delete" type="button" class="btn btn-sm btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;&nbsp;Delete</button>
        </div>
    </div>
</script>

<script type="text/x-tmpl" id="recycle">
    <div class="row">
        <div class="col-sm-6 col-xs-6 margin-5">
            <input type="text" class="form-control" placeholder="Search ....." id="search-in-recycle" />
        </div>
        <div class="col-sm-6 col-xs-6 margin-5">&nbsp;</div>
    
    {% for(var i=0; i<o.length; i++) { %}
        <div class="col-sm-12 col-xs-12 margin-5 container-recycle-user">
            <a href="" class="restore-user" user-id="{%=o[i].user_id%}"><i class="fa fa-fw fa-recycle" style="color: #009E01"></i></a>
            <i class="fa fa-fw fa-user"></i> {%=o[i].user_firstname%} {%=o[i].user_lastname%} - <b>Delete at</b> {%=FormatDateTime(o[i].deleted_at, 'H:i d/m/Y')%}
            <a href="" class="delete-forever-user" user-id="{%=o[i].user_id%}" user-fullname="{%=o[i].user_firstname%} {%=o[i].user_lastname%}"  style="color: #D2322D"><i class="fa fa-fw fa-trash"></i> Delete Forever</a>  
        </div>
    {% } %}
        <div class="col-sm-12 col-xs-12 margin-5 text-right">
            <button id="cancel-recycle" type="button" class="btn btn-sm btn-danger"><i class="fa fa-power-off"></i>&nbsp;&nbsp;&nbsp;Cancel</button>
        </div>
    </div>
</script>

<div class="row">
    <div class="col-xs-12 col-sm-12">
		<div class="box ui-draggable ui-droppable hidden"  id="box-delete-user">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-trash"></i>
					<span>Delete User</span>
				</div>
				<div class="box-icons">
                    <a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                <div class="progress progress-striped active">
					<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                        <span>Loading ....</span>
					</div>
				</div>
                <div class="content"></div>
			</div>
		</div>
	</div>
    
    <div class="col-xs-12 col-sm-12">
		<div class="box ui-draggable ui-droppable hidden"  id="box-edit-user">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-pencil-square-o"></i>
					<span>Edit User</span>
				</div>
				<div class="box-icons">
                    <a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                <div class="progress progress-striped active">
					<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                        <span>Loading ....</span>
					</div>
				</div>
                <div class="content"></div>
			</div>
		</div>
	</div>
    
    <div class="col-xs-12 col-sm-12">
		<div class="box ui-draggable ui-droppable hidden"  id="box-recycle-user">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-recycle"></i>
					<span>Recycle Users</span>
				</div>
				<div class="box-icons">
                    <a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                <div class="progress progress-striped active">
					<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                        <span>Loading ....</span>
					</div>
				</div>
                <div class="content"></div>
			</div>
		</div>
	</div>
    
    <div class="col-xs-12 col-sm-12">
		<div class="box ui-draggable ui-droppable">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-table"></i>
					<span>List Users</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
                <table class="table table-bordered table-striped table-hover table-heading table-datatable dataTable" id="table-users" style="font-size: 13px">
					<thead>
						<tr>
                            <th colspan="2"></th>
							<th>Name</th>
                            <th>Email</th>
                            <th style="min-width: 100px">Status</th>
                            <th>Department</th>
							<th>Created</th>
						</tr>
					</thead>
					<tbody>
                        <?php if($users):?>
                        <?php foreach($users as $user):?>
                        <tr user-id="<?= $user['user_id']?>">
                            <td>
                                <center>
                                    <div class="checkbox-inline">
                                        <label class="text-center">
                                            <input type="checkbox" class="check-user" user-id="<?= $user['user_id']?>">&nbsp;
                                            <i class="fa fa-square-o small"></i>
                                        </label>
                                    </div>
                                </center>
                            </td>
                            <td>
                                <a href="" class="edit-user" user-id="<?= $user['user_id']?>">
                                    <i class="fa fa-fw fa-pencil-square-o"></i> <i>Edit</i>
                                </a>
                                &nbsp;&nbsp;
                                <a href=""class="delete-user" user-id="<?= $user['user_id']?>">
                                    <i class="fa fa-fw fa-trash-o"></i> <i>Delete</i>
                                </a>
                            </td>
							<td><?= $user['user_firstname']?> <?= $user['user_lastname']?></td>
							<td><?= $user['user_email']?></td>
                            <td><input type="checkbox" class="toggle-user-status" user-id="<?= $user['user_id']?>" <?= $user['user_status']=='1'?'checked':''?> /></td>
                            <td>Information Technology</td>
                            <td><?= date('H:i d/m/Y', strtotime($user['created_at']))?></td>
						</tr>
                        <?php endforeach;?>
                        <?php endif;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection