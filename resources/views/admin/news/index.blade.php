@extends('layout.admin')

@section('title', 'Admin - News')

@section('breadcrumb')
<li><a href="<?= route('admin-dashboard')?>">Admin</a></li>
<li><a href="javascript:void()">News</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12">
		<div class="box ui-draggable ui-droppable">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-table"></i>
					<span>List News</span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Title</th>
							<th>Author</th>
							<th>Created</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Firefox</td>
							<td>Mozilla</td>
							<td>Gecko</td>
						</tr>
						<tr>
							<td>2</td>
							<td>Chrome</td>
							<td>Google</td>
							<td>WebKit</td>
						</tr>
						<tr>
							<td>3</td>
							<td>Internet Explorer</td>
							<td>Microsoft</td>
							<td>Trident</td>
						</tr>
						<tr>
							<td>4</td>
							<td>Safari</td>
							<td>Apple</td>
							<td>WebKit</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection