<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Website Template Login</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="<?= asset('public/plugins/bootstrap/css/bootstrap.css" rel="stylesheet')?>">
		<link href="<?= asset('public/plugins/font-awesome-4.4.0/css/font-awesome.min.css')?>" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
		<link href="<?= asset('public/css/style.css')?>" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
				<script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
				<script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
<body>
<div class="container-fluid">
	<div id="page-login" class="row">
		<div class="col-xs-12 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <div class="box">
                <form method="post">
                    <div class="box-content">
                        <div class="text-center">
                            <h3 class="page-header">WeBsite TemPlaTe Admin Login Page</h3>
                        </div>
                        <div id="error" style="color:#D2322D">
                            <?= isset($error)?'<i class="fa fa-exclamation-triangle"></i> ' . $error:''?>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Username</label>
                            <input type="text" class="form-control" name="user_name" value="<?= isset($post)?$post['user_name']:''?>" />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <input type="password" class="form-control" name="user_password" value="<?= isset($post)?$post['user_password']:''?>" />
                        </div>
                        <div class="text-right">
                            <button class="btn btn-primary">Sign in</button>
                        </div>
                    </div>
                </form>
            </div>
		</div>
	</div>
</div>
</body>
</html>