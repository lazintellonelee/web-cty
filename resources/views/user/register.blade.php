<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Website Template User Register</title>
        <meta name="description" content="description">
        <meta name="author" content="Evgeniya">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="<?= asset('public/plugins/bootstrap/css/bootstrap.css') ?>" rel="stylesheet">
        <link href="<?= asset('public/plugins/font-awesome-4.4.0/css/font-awesome.min.css') ?>" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
        <link href="<?= asset('public/css/style.css') ?>" rel="stylesheet">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
                <script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
                <script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container-fluid">
            <div id="page-login" class="row">
                <div class="col-xs-12 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                    <div class="box">
                        <div class="box-content">
                            <form method="post">
                                <div class="text-center">
                                    <h3 class="page-header">WeBsite TemPlaTe Register Page</h3>
                                </div>
                                <div id="error" style="color:#D2322D">
                                    <?= isset($error)?$error:''?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Username</label>
                                    <input type="text" class="form-control" name="user_name" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">E-mail</label>
                                    <input type="text" class="form-control" name="user_email" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Password</label>
                                    <input type="password" class="form-control" name="user_password" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Confirm Password</label>
                                    <input type="password" class="form-control" id="confirm_password" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Firstname</label>
                                    <input type="text" class="form-control" name="user_firstname" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Lastname</label>
                                    <input type="text" class="form-control" name="user_lastname" />
                                </div>
                                <!--
                                <div class="form-group has-error has-feedback">
                                    <label class="control-label" for="inputError2">Username already exists</label>
                                    <input type="text" class="form-control" id="inputError2" aria-describedby="inputError2Status" name="user_name">
                                    <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                    <span id="inputError2Status" class="sr-only">(error)</span>
                                </div>
                                
                                <div class="form-group has-success has-feedback">
                                    <label class="control-label" for="inputSuccess2">Username available</label>
                                    <input type="text" class="form-control" id="inputSuccess2" aria-describedby="inputSuccess2Status" name="user_name">
                                    <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status" class="sr-only">(success)</span>
                                </div>
                                -->
                                <div class="text-right">
                                    <button id="btn-submit" class="btn btn-primary">Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="<?= asset('public/plugins/jquery/jquery-2.1.0.min.js')?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).on('blur', 'input[name="user_name"]', function(event) {
                var _this   = this;
                var text    = $(this).val();
                if(text.trim() === '') {
                    $(_this).parents('div.form-group').replaceWith('<div class="form-group"> <label class="control-label">Username</label> <input type="text" class="form-control" name="user_name" /> </div>');
                    return true;
                }
                $('#btn-submit').prop('disabled', true);
                var text    = $(_this).val();
                var http    = new XMLHttpRequest();
                var form    = new FormData();
                form.append('user_name', text);
                http.open('POST', '<?= url() ?>/user-check-exist', true);
                http.onload = function(event) {
                    //console.log(this.responseText);return true;
                    $('#btn-submit').prop('disabled', false);
                    var result = JSON.parse(this.responseText);
                    if(result.exist !== undefined) {
                        $(_this).parents('div.form-group').replaceWith('<div class="form-group has-error has-feedback"> <label class="control-label" for="inputError2">Username already exists</label> <input type="text" class="form-control" id="inputError2" aria-describedby="inputError2Status" name="user_name" value="' + text + '"> <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span> <span id="inputError2Status" class="sr-only">(error)</span> </div>');
                        return true;
                    }
                    $(_this).parents('div.form-group').replaceWith('<div class="form-group has-success has-feedback"> <label class="control-label" for="inputSuccess2">Username available</label> <input type="text" class="form-control" id="inputSuccess2" aria-describedby="inputSuccess2Status" name="user_name" value="' + text + '"> <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span> <span id="inputSuccess2Status" class="sr-only">(success)</span> </div>');
                    return true;
                };
                http.send(form);
            });

            $(document).on('blur', 'input[name="user_email"]', function(event) {
                
            
                var _this   = this;
                var text    = $(this).val();
                if(text.trim() === '') {
                    $(_this).parents('div.form-group').replaceWith('<div class="form-group"> <label class="control-label">E-mail</label> <input type="text" class="form-control" name="user_email" /> </div>');
                    return true;
                }
                $('#btn-submit').prop('disabled', true);
                var text    = $(_this).val();
                var http    = new XMLHttpRequest();
                var form    = new FormData();
                form.append('user_email', text);
                http.open('POST', '<?= url() ?>/user-check-email-exist', true);
                http.onload = function(event) {
                    //console.log(this.responseText);return true;
                    $('#btn-submit').prop('disabled', false);
                    var result = JSON.parse(this.responseText);
                    if(result.exist !== undefined) {
                        $(_this).parents('div.form-group').replaceWith('<div class="form-group has-error has-feedback"> <label class="control-label" for="inputError2">E-mail already exists</label> <input type="text" class="form-control" id="inputError2" aria-describedby="inputError2Status" name="user_email" value="' + text + '"> <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span> <span id="inputError2Status" class="sr-only">(error)</span> </div>');
                        return true;
                    }
                    $(_this).parents('div.form-group').replaceWith('<div class="form-group has-success has-feedback"> <label class="control-label" for="inputSuccess2">E-mail available</label> <input type="text" class="form-control" id="inputSuccess2" aria-describedby="inputSuccess2Status" name="user_email" value="' + text + '"> <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span> <span id="inputSuccess2Status" class="sr-only">(success)</span> </div>');
                    return true;
                };
                http.send(form);
            });
            
            $('form').on('keypress', function(event) {
                if(event.keyCode === 13) {
                    event.preventDefault();
                }
            });
            
            $('#btn-submit').on('click', function(event) {
                event.preventDefault();
                
                
                if($('input[name="user_name"]').val().trim() === '') {
                    $('#error').html('<i class="fa fa-circle"></i> We need your login name.');
                    $('input[name="user_name"]').focus();
                    return true;
                }
                
                if($('input[name="user_email"]').val().trim() === '') {
                    $('#error').html('<i class="fa fa-circle"></i> We need your email.');
                    $('input[name="user_email"]').focus();
                    return true;
                }
                
                if($('input[name="user_password"]').val().trim() === '') {
                    $('#error').html('<i class="fa fa-circle"></i> You need a password to privacy.');
                    $('input[name="user_password"]').focus();
                    return true;
                }
                
                if($('input#confirm_password').val().trim() === '') {
                    $('#error').html('<i class="fa fa-circle"></i> You need a password to privacy.');
                    $('input#confirm_password').focus();
                    return true;
                }
                
                if($('input[name="user_firstname"]').val().trim() === '') {
                    $('#error').html('<i class="fa fa-circle"></i> You need to have a name.');
                    $('input[name="user_firstname"]').focus();
                    return true;
                }
                
                if($('input[name="user_lastname"]').val().trim() === '') {
                    $('#error').html('<i class="fa fa-circle"></i> You need to have a name.');
                    $('input[name="user_lastname"]').focus();
                    return true;
                }
                
                var password            = $('input[name="user_password"]').val();
                var confirm_password    = $('input#confirm_password').val();
                if(password !== confirm_password) {
                    $('#error').html('<i class="fa fa-circle"></i> Confirm password wrong.');
                    $('input#confirm_password').focus();
                    return true;
                }
                
                if($('div.form-group').hasClass('has-error')) {
                    $('#error').html('<i class="fa fa-circle"></i> Something wrong here.');
                    return true;
                }
                
                $('form')[0].submit();
                return true;
            });
        });
    </script>
</html>