@extends('layout.user')

@section('title', 'Profile')

@section('breadcrumb')
<li><a href="<?= route('home')?>">Home</a></li>
<li><a href="javascript::void()">Profile</a></li>
@endsection