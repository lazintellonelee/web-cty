<html>
    <head>
        <title>
            @yield('title')
        </title>
        <link href="<?= asset('public/plugins/bootstrap/css/bootstrap.css')?>" rel="stylesheet">
		<link href="<?= asset('public/plugins/jquery-ui/jquery-ui.min.css')?>" rel="stylesheet">
		<link href="<?= asset('public/plugins/font-awesome-4.4.0/css/font-awesome.min.css')?>" rel="stylesheet">
		<link href='<?= asset('public/css/righteous.css')?>' rel='stylesheet' type='text/css'>
		<link href="<?= asset('public/plugins/fancybox/jquery.fancybox.css')?>" rel="stylesheet">
		<link href="<?= asset('public/plugins/fullcalendar/fullcalendar.css')?>" rel="stylesheet">
		<link href="<?= asset('public/plugins/xcharts/xcharts.min.css')?>" rel="stylesheet">
		<link href="<?= asset('public/plugins/select2/select2.css')?>" rel="stylesheet">
        <link href="<?= asset('public/css/animate.css')?>" rel="stylesheet">
		<link href="<?= asset('public/css/style.css')?>" rel="stylesheet">
        @yield('headstyle')
        
        <script src="<?= asset('public/plugins/jquery/jquery-2.1.0.min.js')?>"></script>
        <script src="<?= asset('public/plugins/jquery-ui/jquery-ui.min.js')?>"></script>
        @yield('headscript')
        
    </head>
    <body>
        <div id="screensaver">
            <canvas id="canvas"></canvas>
            <i class="fa fa-lock" id="screen_unlock"></i>
        </div>
        <div id="modalbox">
            <div class="devoops-modal">
                <div class="devoops-modal-header">
                    <div class="modal-header-name">
                        <span>Basic table</span>
                    </div>
                    <div class="box-icons">
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="devoops-modal-inner">
                </div>
                <div class="devoops-modal-bottom">
                </div>
            </div>
        </div>
        <header class="navbar">
            <div class="container-fluid expanded-panel">
                <div class="row">
                    <div id="logo" class="col-xs-12 col-sm-2">
                        <a href="index.html">Admin Page</a>
                    </div>
                    <div id="top-panel" class="col-xs-12 col-sm-10">
                        <div class="row">
                            <div class="col-xs-8 col-sm-4">
                                <a href="#" class="show-sidebar" style="margin-top: 16px">
                                    <i class="fa fa-bars"></i>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-8 top-panel-right">
                                <ul class="nav navbar-nav pull-right panel-menu">
                                    <!-- INFO USER -->
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle account" data-toggle="dropdown">
                                            <div class="avatar">
                                                <img src="<?= get_admin_avatar()?>" class="img-rounded" alt="avatar" />
                                            </div>
                                            <i class="fa fa-angle-down pull-right"></i>
                                            <div class="user-mini pull-right">
                                                <span class="welcome">Welcome,</span>
                                                <span><?= get_admin_full_name()?></span>
                                            </div>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="<?= route('user')?>">
                                                    <i class="fa fa-home"></i>
                                                    <span>Go Home</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-cog"></i>
                                                    <span>Settings</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= route('admin-logout')?>">
                                                    <i class="fa fa-power-off"></i>
                                                    <span>Logout</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--End Header-->
        <!--Start Container-->
        <div id="main" class="container-fluid">
            <div class="row">
                <div id="sidebar-left" class="col-xs-2 col-sm-2">
                    <ul class="nav main-menu">
                        <li>
                            <a href="<?= route('admin-dashboard')?>">
                                <i class="fa fa-dashboard"></i>
                                <span class="hidden-xs">Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= route('admin-users-management')?>">
                                <i class="fa fa-users"></i>
                                <span class="hidden-xs">Users</span>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle">
                                <i class="fa fa-newspaper-o"></i>
                                <span class="hidden-xs">News</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="ajax-link" href="ajax/charts_xcharts.html">xCharts</a></li>
                                <li><a class="ajax-link" href="ajax/charts_flot.html">Flot Charts</a></li>
                                <li><a class="ajax-link" href="ajax/charts_google.html">Google Charts</a></li>
                                <li><a class="ajax-link" href="ajax/charts_morris.html">Morris Charts</a></li>
                                <li><a class="ajax-link" href="ajax/charts_coindesk.html">CoinDesk realtime</a></li>
                            </ul>
                        </li>
                        <!--
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle">
                                <i class="fa fa-bar-chart-o"></i>
                                <span class="hidden-xs">Charts</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="ajax-link" href="ajax/charts_xcharts.html">xCharts</a></li>
                                <li><a class="ajax-link" href="ajax/charts_flot.html">Flot Charts</a></li>
                                <li><a class="ajax-link" href="ajax/charts_google.html">Google Charts</a></li>
                                <li><a class="ajax-link" href="ajax/charts_morris.html">Morris Charts</a></li>
                                <li><a class="ajax-link" href="ajax/charts_coindesk.html">CoinDesk realtime</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle">
                                <i class="fa fa-table"></i>
                                <span class="hidden-xs">Tables</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="ajax-link" href="ajax/tables_simple.html">Simple Tables</a></li>
                                <li><a class="ajax-link" href="ajax/tables_datatables.html">Data Tables</a></li>
                                <li><a class="ajax-link" href="ajax/tables_beauty.html">Beauty Tables</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle">
                                <i class="fa fa-pencil-square-o"></i>
                                <span class="hidden-xs">Forms</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="ajax-link" href="ajax/forms_elements.html">Elements</a></li>
                                <li><a class="ajax-link" href="ajax/forms_layouts.html">Layouts</a></li>
                                <li><a class="ajax-link" href="ajax/forms_file_uploader.html">File Uploader</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle">
                                <i class="fa fa-desktop"></i>
                                <span class="hidden-xs">UI Elements</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="ajax-link" href="ajax/ui_grid.html">Grid</a></li>
                                <li><a class="ajax-link" href="ajax/ui_buttons.html">Buttons</a></li>
                                <li><a class="ajax-link" href="ajax/ui_progressbars.html">Progress Bars</a></li>
                                <li><a class="ajax-link" href="ajax/ui_jquery-ui.html">Jquery UI</a></li>
                                <li><a class="ajax-link" href="ajax/ui_icons.html">Icons</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle">
                                <i class="fa fa-list"></i>
                                <span class="hidden-xs">Pages</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="ajax/page_login.html">Login</a></li>
                                <li><a href="ajax/page_register.html">Register</a></li>
                                <li><a id="locked-screen" class="submenu" href="ajax/page_locked.html">Locked Screen</a></li>
                                <li><a class="ajax-link" href="ajax/page_contacts.html">Contacts</a></li>
                                <li><a class="ajax-link" href="ajax/page_feed.html">Feed</a></li>
                                <li><a class="ajax-link add-full" href="ajax/page_messages.html">Messages</a></li>
                                <li><a class="ajax-link" href="ajax/page_pricing.html">Pricing</a></li>
                                <li><a class="ajax-link" href="ajax/page_invoice.html">Invoice</a></li>
                                <li><a class="ajax-link" href="ajax/page_search.html">Search Results</a></li>
                                <li><a class="ajax-link" href="ajax/page_404.html">Error 404</a></li>
                                <li><a href="ajax/page_500.html">Error 500</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle">
                                <i class="fa fa-map-marker"></i>
                                <span class="hidden-xs">Maps</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="ajax-link" href="ajax/maps.html">OpenStreetMap</a></li>
                                <li><a class="ajax-link" href="ajax/map_fullscreen.html">Fullscreen map</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle">
                                <i class="fa fa-picture-o"></i>
                                <span class="hidden-xs">Gallery</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="ajax-link" href="ajax/gallery_simple.html">Simple Gallery</a></li>
                                <li><a class="ajax-link" href="ajax/gallery_flickr.html">Flickr Gallery</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="ajax-link" href="ajax/typography.html">
                                <i class="fa fa-font"></i>
                                <span class="hidden-xs">Typography</span>
                            </a>
                        </li>
                        <li>
                            <a class="ajax-link" href="ajax/calendar.html">
                                <i class="fa fa-calendar"></i>
                                <span class="hidden-xs">Calendar</span>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle">
                                <i class="fa fa-picture-o"></i>
                                <span class="hidden-xs">Multilevel menu</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">First level menu</a></li>
                                <li><a href="#">First level menu</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle">
                                        <i class="fa fa-plus-square"></i>
                                        <span class="hidden-xs">Second level menu group</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Second level menu</a></li>
                                        <li><a href="#">Second level menu</a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle">
                                                <i class="fa fa-plus-square"></i>
                                                <span class="hidden-xs">Three level menu group</span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Three level menu</a></li>
                                                <li><a href="#">Three level menu</a></li>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle">
                                                        <i class="fa fa-plus-square"></i>
                                                        <span class="hidden-xs">Four level menu group</span>
                                                    </a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Four level menu</a></li>
                                                        <li><a href="#">Four level menu</a></li>
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle">
                                                                <i class="fa fa-plus-square"></i>
                                                                <span class="hidden-xs">Five level menu group</span>
                                                            </a>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#">Five level menu</a></li>
                                                                <li><a href="#">Five level menu</a></li>
                                                                <li class="dropdown">
                                                                    <a href="#" class="dropdown-toggle">
                                                                        <i class="fa fa-plus-square"></i>
                                                                        <span class="hidden-xs">Six level menu group</span>
                                                                    </a>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="#">Six level menu</a></li>
                                                                        <li><a href="#">Six level menu</a></li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li><a href="#">Three level menu</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        -->
                    </ul>
                </div>
                <!--Start Content-->
                <div id="content" class="col-xs-12 col-sm-10">
                    <div id="ajax-content">
                        
                        <div class="row">
                            
                            <div id="breadcrumb" class="col-xs-12">
                                <ol class="breadcrumb">
                                    @section('breadcrumb')
                                    <li><a href="<?= route('admin-dashboard')?>">Dashboard</a></li>
                                    @show
                                </ol>
                            </div>
                        </div>
                        @show
                        
                        @yield('content')
                    </div>
                </div>
                <!--End Content-->
            </div>
        </div>
        <script src="<?= asset('public/plugins/noty/js/noty/packaged/jquery.noty.packaged.min.js')?>"></script>
        <script src='<?= asset('public/js/functions.js')?>'></script>
        <script src="<?= asset('public/plugins/bootstrap/js/bootstrap.min.js')?>"></script>
        <script src="<?= asset('public/plugins/justified-gallery/jquery.justifiedgallery.min.js')?>"></script>
        <script src="<?= asset('public/plugins/tinymce/tinymce.min.js')?>"></script>
        <script src="<?= asset('public/plugins/tinymce/jquery.tinymce.min.js')?>"></script>
        <script src="<?= asset('public/js/devoops.js?url=' . url())?>"></script>
        <script>
            $(document).ready(function() {
                // Drag-n-Drop feature
                WinMove();
                
                $('.main-menu li').map(function() {
                    var li = $(this).children('a'); 
                    if(li.attr('href') === location.href) {
                        $(this).addClass('active');
                    }
                });
            });
        </script>
        @yield('inlinescript')
    </body>
</html>