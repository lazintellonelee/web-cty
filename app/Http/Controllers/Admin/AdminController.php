<?php
    namespace App\Http\Controllers\Admin;
    
    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use App\Models\Users;
    use Symfony\Component\HttpFoundation\Cookie;
    
    class AdminController extends Controller {
        
        public function index(Request $request) {
            return view('admin.dashboard');
        }
        
        public function login(Request $request) {
            if($request->isMethod('post')) {
                $data   = $request->all();
                $_user  = Users::where('user_name', $data['user_name'])->first();
                if(!$_user) {
                    return view('admin.login.index', array(
                        'error' => 'Username does not exist',
                        'post'  => $data
                    ));
                }
                
                $user = $_user->toArray();
                if($user['user_role'] !== 'admin') {
                    return view('admin.login.index', array(
                        'error' => 'You are not allowed here',
                        'post'  => $data
                    ));
                }
                
                if($user['user_password'] !== md5($data['user_password'] . $user['user_salt'])) {
                    return view('admin.login.index', array(
                        'error' => 'Incorrect password',
                        'post'  => $data
                    ));
                }
                
                $request->session()->push('admin', array(
                    'user_id'           => $user['user_id'],
                    'user_name'         => $user['user_name'],
                    'user_firstname'    => $user['user_firstname'],
                    'user_lastname'     => $user['user_lastname'],
                    'user_avatar'       => $user['user_avatar'],
                    'user_role'         => $user['user_role']
                ));
                return redirect()->route('admin-dashboard');
            }
            return view('admin.login.index');
        }
        
        public function logout(Request $request) {
            $request->session()->clear();
            return redirect()->route('admin-dashboard');
        }
    }
?>
