<?php
    namespace App\Http\Controllers\Admin;
    
    use App\Http\Controllers\Controller;
    
    class AdminNewsController extends Controller {
        public function index() {
            return view('admin.news.index');
        }
    }
?>