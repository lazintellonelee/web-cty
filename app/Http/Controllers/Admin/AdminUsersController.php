<?php
    namespace App\Http\Controllers\Admin;
    
    use App\Models\Users;
    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request as _Request;
    use Request;
    
    class AdminUsersController extends Controller {
        public function index() {
            return view('admin.users.index', array(
                'users' => Users::where('deleted_at', NULL)
                                ->where('user_role', 'user')
                                ->orderBy('user_id', 'desc')
                                ->get(array(
                                    'user_id',
                                    'user_firstname',
                                    'user_lastname',
                                    'user_email',
                                    'user_status',
                                    'created_at'
                                ))->toArray()
            ));
        }
        
        public function changestatus() {
            $data = Request::all();
            Users
            ::where('user_id', $data['user_id'])
            ->update(['user_status' => $data['status']]);
            exit();
        }
        
        public function get() {
            $user_id = Request::input('user_id');
            echo Users::find($user_id)->toJson();
            exit();
        }
        
        public function gets() {
            $users_id = Request::input('users_id');
            echo(Users::whereIn('user_id', $users_id)->get(array(
                'user_id',
                'user_firstname',
                'user_lastname'
            ))->toJson());
            exit();
        }
        
        public function edit() {
            $data                   = Request::all();
            $user_id                = $data['user_id'];unset($data['user_id']);
            $data['user_birthday']  = date(
                'Y-m-d', 
                strtotime(str_replace('/', '-', $data['user_birthday']))
            );
            
            if(Users::where('user_id', $user_id)
                    ->update($data)) {
                echo Users::find($user_id)->toJson();
                exit();
            }
            
            echo json_encode(array(
                'error'     => 1,
                'messenger' => 'Update user unsuccess! Please try again.'
            ));
            exit();
        }
        
        public function delete() {
            $users_id   = Request::input('users_id');
            
            $users      = Users::whereIn('user_id', $users_id)->get(array(
                'user_firstname',
                'user_lastname'
            ))->toJson();
            
            if(!Users::whereIn('user_id', $users_id)
                    ->update(array(
                        'deleted_at' => date('Y-m-d H:i:s', time())
                    ))) {
                echo json_encode(array(
                    'error'     => 1,
                    'messenger' => 'Delete user unsuccess! Please try again.'
                ));
                exit();
            }
            
            echo $users;
            exit();
        }
        
        public function getdeleted() {
            echo Users::where('deleted_at', '<>', 'NULL')
                      ->where('user_role', 'user')
                      ->orderBy('deleted_at', 'desc')
                      ->get(array(
                          'user_id',
                          'user_firstname',
                          'user_lastname',
                          'user_email',
                          'user_status',
                          'deleted_at'
                      ))->toJson();
            exit();
        }
        
        public function restore() {
            $user_id = Request::input('user_id');
            $user    = Users::find($user_id)->toArray();
            if(!Users::where('user_id', $user_id)
                     ->update([
                         'deleted_at' => NULL
                     ])) {
                echo json_encode(array(
                    'error'     => 1,
                    'messenger' => 'Restore user ' . $user['user_firstname'] . ' ' . $user['user_lastname'] . ' unsuccess.'
                ));
                exit();
            }
            
            echo json_encode($user);
            exit();
        }
    }
?>