<?php
    namespace App\Http\Controllers\Plan;
    
    use App\Http\Controllers\Controller;
    
    class IndexController extends Controller {
        public function index() {
            return view('plan.index');
        }
    }
?>