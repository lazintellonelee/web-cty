<?php
    namespace App\Http\Controllers\User;
    
    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Request as _Request;
    use App\Models\Users;
    
    class UserController extends Controller {
        public function index(Request $request) {
            return view('user.index');
        }
        
        public function profile(Request $request) {
            return view('user.profile');
        }
        
        public function register(Request $request) {
            if($request->session()->has('user')) {
               return redirect()->route('user'); 
            }
            
            if($request->isMethod('post')) {
                $data                   = $request->all();
                $data['user_salt']      = md5($data['user_password'] . uniqid());
                $data['user_password']  = md5($data['user_password'] . $data['user_salt']);
                
                $user = Users::create($data)->toArray();
                return redirect()->route('register-success')->with('register_user', Users::where('user_id', $user['user_id'])->first()->toArray());
            }
            
            return view('user.register');
        }
        
        public function registerSuccess(Request $request) {
            if(!$request->session()->has('register_user')) {
                return redirect()->route('user');
            }
            
            $user = $request->session()->get('register_user');
            $request->session()->forget('register_user');
            $request->session()->push('user', array(
                'user_id'           => $user['user_id'],
                'user_name'         => $user['user_name'],
                'user_firstname'    => $user['user_firstname'],
                'user_lastname'     => $user['user_lastname'],
                'user_avatar'       => $user['user_avatar'],
                'user_role'         => $user['user_role']
            ));
            return view('user.register_success', array(
                'user_name'         => $user['user_name'],
                'user_firstname'    => $user['user_firstname'],
                'user_lastname'     => $user['user_lastname'],
            ));
        }
        
        public function login(Request $request) {
            if($request->session()->has('user')) {
                return redirect()->route('user');
            }
            
            if($request->isMethod('post')) {
                $data   = $request->all();
                $_user  = Users::where('user_name', $data['user_name'])->first();
                if(!$_user) {
                    return view('admin.login.index', array(
                        'error' => 'Username does not exist',
                        'post'  => $data
                    ));
                }
                
                $user = $_user->toArray();
                if($user['user_password'] !== md5($data['user_password'] . $user['user_salt'])) {
                    return view('admin.login.index', array(
                        'error' => 'Incorrect password',
                        'post'  => $data
                    ));
                }
                
                if($user['user_status'] === 0) {
                    return view('admin.login.index', array(
                        'error' => 'Your account has not been activated',
                        'post'  => $data
                    ));
                }
                
                $request->session()->push('user', array(
                    'user_id'           => $user['user_id'],
                    'user_name'         => $user['user_name'],
                    'user_firstname'    => $user['user_firstname'],
                    'user_lastname'     => $user['user_lastname'],
                    'user_avatar'       => $user['user_avatar'],
                    'user_role'         => $user['user_role']
                ));
                return redirect()->route('user');
            }
            
            return view('user.login');
        }
        
        public function logout(Request $request) {
            if(!$request->session()->has('user')) {
                return redirect()->route('login');
            }
            $request->session()->clear();
            return redirect()->route('user');
        }
        
        public function check() {
            $username = _Request::input('user_name');
            if(count(Users::where('user_name', $username)->get()->toArray()) !== 0) {
                echo json_encode(array(
                    'exist' => 1
                ));
                exit();
            }
            echo json_encode(array());
            exit();
        }
        
        public function checkEmail() {
            $useremail = _Request::input('user_email');
            if(count(Users::where('user_email', $useremail)->get()->toArray()) !== 0) {
                echo json_encode(array(
                    'exist' => 1
                ));
                exit();
            }
            echo json_encode(array());
            exit();
        }
    }
?>