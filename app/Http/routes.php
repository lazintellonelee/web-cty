<?php
    //Route Home
    Route::get('/', ['as' => 'home', 'uses' => 'User\UserController@index']);
    //Route Home
    
    //Route User
    Route::group([
        'middleware'    => 'user.auth',
        'prefix'        => 'user'
    ], function() {
        Route::get('/', ['as' => 'user', 'uses' => 'User\UserController@index']);
        Route::get('profile', ['as' => 'profile', 'uses' => 'User\UserController@profile']);
    });
    //Route User
    
    //Route User Register
    Route::match(['get', 'post'], 'register', ['as' => 'register', 'uses' => 'User\UserController@register']);
    //Route User Register
    
    //Route User Register
    Route::get('register-success', ['as' => 'register-success', 'uses' => 'User\UserController@registerSuccess']);
    //Route User Register

    //Route User Login
    Route::match(['get', 'post'], 'login', ['as' => 'login', 'uses' => 'User\UserController@login']);
    //Route User Login
    
    //Route User Logout
    Route::get('logout', ['as' => 'logout', 'uses' => 'User\UserController@logout']);
    //Route User Logout

    //Route User Check Exists
    Route::post('user-check-exist', ['as' => 'user-check-exist', 'uses' => 'User\UserController@check']);
    Route::post('user-check-email-exist', ['as' => 'user-check-email-exist', 'uses' => 'User\UserController@checkEmail']);
    //Route User Check Exists
    
    //Route Plan
    Route::group([
        'middleware'    => 'user.auth',
        'prefix'        => 'plan'
    ], function() {
        Route::get('/', ['as' => 'plan', 'uses' => 'Plan\IndexController@index']);
    });
    //Route Plan

    Route::group([
        'middleware'    => 'admin.auth',
        'prefix'        => 'admin'
            ], function() {
        Route::get('/', ['as' => 'admin-dashboard', 'uses' => 'Admin\AdminController@index']);
        
        //Route Admin Logout
        Route::get('/logout', ['as' => 'admin-logout', 'uses' => 'Admin\AdminController@logout']);
        //Route Admin Logout
        
        //Route Admin Users
        Route::group([
            'prefix' => 'users'
                ], function() {
            Route::get('/', [
                'as'    => 'admin-users-management',
                'uses'  => 'Admin\AdminUsersController@index'
            ]);

            Route::post('change-status', [
                'as'    => 'admin-users-change-status',
                'uses'  => 'Admin\AdminUsersController@changestatus'
            ]);

            Route::post('get-user', [
                'as'    => 'admin-users-get-user',
                'uses'  => 'Admin\AdminUsersController@get'
            ]);

            Route::post('get-users', [
                'as'    => 'admin-users-get-users',
                'uses'  => 'Admin\AdminUsersController@gets'
            ]);

            Route::post('edit-user', [
                'as'    => 'admin-users-edit-user',
                'uses'  => 'Admin\AdminUsersController@edit'
            ]);

            Route::post('delete-user', [
                'as'    => 'admin-users-delete-user',
                'uses'  => 'Admin\AdminUsersController@delete'
            ]);

            Route::post('get-users-deleted', [
                'as'    => 'admin-users-get-users-deleted',
                'uses'  => 'Admin\AdminUsersController@getdeleted'
            ]);

            Route::post('restore-user', [
                'as'    => 'admin-user-restore',
                'uses' => 'Admin\AdminUsersController@restore'
            ]);
        });
        //Route Admin Users
    });

    //Route Admin Login
    Route::match(['get', 'post'], 'admin/login', ['as' => 'admin-login', 'uses' => 'Admin\AdminController@login']);
    //Route Admin Login
?>
