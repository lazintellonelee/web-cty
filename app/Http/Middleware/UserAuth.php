<?php
    namespace App\Http\Middleware;
    
    use Closure;
    
    class UserAuth {
        public function handle($request, Closure $next) {
            if(!$request->session()->has('user')) {
                return redirect()->route('login');
            }
            return $next($request);
        }
    }
?>