<?php
    namespace App\Models;
    
    use Illuminate\Database\Eloquent\Model;
    
    class Users extends Model {
        protected $table        = "tbl_users";
        protected $primaryKey   = "user_id";
        protected $fillable     = array(
            'user_name',
            'user_firstname',
            'user_lastname',
            'user_email',
            'user_password',
            'user_salt'
        );
        public $timestamps      = true;
    }
?>