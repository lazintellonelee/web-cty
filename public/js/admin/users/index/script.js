$(document).ready(function () {
    $('.toggle-user-status').bootstrapToggle({
        on          : 'Active',
        off         : 'Inactive',
        onstyle     : 'success',
        offstyle    : 'danger',
        width       : 90,
        size        : 'mini'
    });
    
    //Config dataTable
    var oTable = $('#table-users').DataTable({
        "aoColumnDefs"      : [{
            "aTargets"  : [0, 1, 4],
            "bSortable" : false
        }],
        "aaSorting"         : [[0, "asc"]],
        "sDom"              : "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
        "sPaginationType"   : "bootstrap",
        "aLengthMenu"       : [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]],
        "oLanguage"         : {
            "sSearch"               : '',
            "sLengthMenu"           : '_MENU_',
            "sEmptyTable"           : '<center><b>No data!!</b></center>',
            "sZeroRecords"          : '<center><b>No data !!</b></center>',
            "sInfo"                 : '',
            "sInfoFiltered"         : '',
            "sInfoEmpty"            : ''
        },
        bAutoWidth: false
    });
    $('.dataTables_filter input').attr('placeholder','Search ...');
    $('.dataTables_length select').addClass('form-control input-sm');
    $('.dataTables_wrapper div.box-content').css({
        'padding' : '15 0 15 0'
    });
    $('.dataTables_wrapper div.box-content div:nth-child(1)').css({
       'padding-left'  : 0
    });
    $('.dataTables_wrapper div.box-content div:nth-child(2)').css({
       'padding-right'  : 0
    });
    //Config dataTable
    
    $('#table-users').after(tmpl('tools', {}));
    
    $(document).on('click', '#check-all-users', function(event) {
        CheckAll(event.target.id, 'check-user');
    });
    
    //Change status
    var ChangeStatusTimeOut;
    $(document).on('change', '.toggle-user-status', function(event) {
        var _this   = this;
        var user_id = $(this).attr('user-id');
        var status  = $(this).prop('checked')?'1':'0';
        var form    = new FormData();
        var http    = new XMLHttpRequest();
        
        $('.toggle-user-status').not(this).prop('disabled', true).parent('div').children('div.toggle-group').css({
            'opacity'   : 0.8
        });
        
        clearTimeout(ChangeStatusTimeOut);
        ChangeStatusTimeOut = setTimeout(function () {
            form.append('status', status);
            form.append('user_id', user_id);

            http.open('POST', params.url + "/admin/users/change-status", true);
            http.onload = function (event) {
                $('.toggle-user-status').not(_this).prop('disabled', false).parent('div').children('div.toggle-group').css({
                    'opacity': 1
                });
                return true;
            };

            http.send(form);
        }, 1000);
    });
    //Change status
    
    //Edit user
    var ScrollTop = function() {
        $(document).scrollTop(0);
    };  
    
    var CallBoxEdit = function() {
        $('#box-edit-user .box-content .content').html('');
        $('#box-edit-user').removeClass('hidden').addClass('animated bounceIn');
        setTimeout(function(){
            $('#box-edit-user').removeClass('animated bounceIn');
        }, 1000);
        return true;
    };
    
    var CloseEdit = function() {
        $('#box-edit-user').removeClass('hidden').addClass('animated bounceOut');
        setTimeout(function(){
            $('#box-edit-user').removeClass('animated bounceOut').addClass('hidden');
            $('#box-edit-user .box-content .content').html('');
        }, 1000);
    };
    
    var CallProgressLoadEdit = function() {
        $('#box-edit-user .box-content .progress').removeClass('hidden');
    };
    
    var CallCloseProgressLoadEdit = function() {
        $('#box-edit-user .box-content .progress').addClass('hidden');
    };
    
    var GetContentEdit = function(target) {
        var user_id = $(target).attr('user-id');
        var http    = new XMLHttpRequest();
        var form    = new FormData();
        
        form.append('user_id', user_id);
        http.open('POST', params.url + "/admin/users/get-user", true);
        http.onload = function(event) {
            CallCloseProgressLoadEdit();
            var result = JSON.parse(this.responseText);
            $('#box-edit-user .box-content .content').html(tmpl('edit', result));
            $('#form-edit select#user_sex').val(result.user_sex);
            ScrollTop();
        };
        http.send(form);
    };
    
    $(document).on('click', '.edit-user', function(event) {
        event.preventDefault();
        CallBoxEdit();
        CallProgressLoadEdit();
        GetContentEdit(this);
        return true;
    });
    
    $(document).on('click', '#cancel-edit', function(event) {
        CloseEdit();
    });
    
    $(document).on('click', '#btn-edit', function(event) {
        CallProgressLoadEdit();
        var http    = new XMLHttpRequest();
        var form    = new FormData($('form#form-edit')[0]);
        http.open('POST', params.url + '/admin/users/edit-user', true);
        http.onload = function(event) {
            CallCloseProgressLoadEdit();
            var result = JSON.parse(this.responseText);
            
            if(result.error !== undefined) {
                CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> ' + result.messenger);
                return true;
            }
            CloseEdit();
            CallNoty('success', '<i class="fa fa-check"></i> Update user ' + result.user_firstname + ' ' + result.user_lastname + ' success.');
            $('tr[user-id="' + result.user_id + '"]').html(tmpl('tr-edit', result));
            $('tr[user-id="' + result.user_id + '"] input.toggle-user-status').bootstrapToggle({
                on: 'Active',
                off: 'Inactive',
                onstyle: 'success',
                offstyle: 'danger',
                width: 100,
                size: 'mini'
            });
        };
        http.send(form);
    });
    //Edit user
    
    //Delete user
    var ArrayDeleteUser = [];
    
    var ClearArrarDeleteUser = function() {
        ArrayDeleteUser.length = 0;
    };
    
    var CallBoxDelete = function() {
        $('#box-delete-user .box-content .content').html('');
        $('#box-delete-user').removeClass('hidden').addClass('animated bounceIn');
        setTimeout(function(){
            $('#box-delete-user').removeClass('animated bounceIn');
        }, 1000);
        return true;
    };
    
    var CloseDelete = function() {
        $('#box-delete-user').removeClass('hidden').addClass('animated bounceOut');
        setTimeout(function(){
            $('#box-delete-user').removeClass('animated bounceOut').addClass('hidden');
            $('#box-delete-user .box-content .content').html('');
        }, 1000);
    };
    
    var CallProgressLoadDelete = function() {
        $('#box-delete-user .box-content .progress').removeClass('hidden');
    };
    
    var CallCloseProgressLoadDelete = function() {
        $('#box-delete-user .box-content .progress').addClass('hidden');
    };
    
    var BuildContentDelete = function() {
        ScrollTop();
        
        var http    = new XMLHttpRequest();
        var form    = new FormData();
        for(var i=0; i<ArrayDeleteUser.length; i++) {
            form.append('users_id[]', ArrayDeleteUser[i]);
        }
        http.open('POST', params.url + '/admin/users/get-users', true);
        http.onload = function(event) {
            var result = JSON.parse(this.responseText);
            CallCloseProgressLoadDelete();
            $('#box-delete-user .box-content .content').html(tmpl('delete', result));
            return true;
        };
        http.send(form);
    };
    
    $(document).on('click', '.delete-user', function(event) {
        event.preventDefault();
        ClearArrarDeleteUser();
        CallBoxDelete();
        var user_id = $(this).attr('user-id');
        ArrayDeleteUser[ArrayDeleteUser.length] = user_id;
        BuildContentDelete();
    });
    
    $(document).on('click', '#delete-check-users', function(event) {
        event.preventDefault();
        if($('input.check-user:checked').length === 0) {
            CallNoty('warning', '<i class="fa fa-fw fa-exclamation-triangle"></i> You did not select any user! What do you want to delete?');
            return true;
        }
        
        ClearArrarDeleteUser();
        CallBoxDelete();
        $('input.check-user:checked').map(function() {
            ArrayDeleteUser[ArrayDeleteUser.length] = $(this).attr('user-id');
        });
        BuildContentDelete();
    });
    
    $(document).on('click', '#cancel-delete', function(event) {
        CloseDelete();
    });
    
    $(document).on('click', '.remove-delete-user', function(event) {
        event.preventDefault();
        var div     = $(this).parent('div');
        var user_id = $(this).attr('user-id');
        ArrayDeleteUser.splice(ArrayDeleteUser.indexOf(user_id), 1);
        div.addClass('animated bounceOutRight');
        setTimeout(function(){
            div.remove();
            if($('#box-delete-user').find('.remove-delete-user').length === 0) {
                CloseDelete();
            }
        }, 500);
        return true;
    });
    
    $(document).on('click', '#btn-delete', function(event) {
        CallProgressLoadDelete();
        
        var http    = new XMLHttpRequest();
        var form    = new FormData();
        for(var i=0; i<ArrayDeleteUser.length; i++) {
            form.append('users_id[]', ArrayDeleteUser[i]);
        }
        
        http.open('POST', params.url + '/admin/users/delete-user');
        http.onload = function(event) {
            CallCloseProgressLoadDelete();
            var result = JSON.parse(this.responseText);
            if(result.error !== undefined) {
                CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> ' + result.messenger);
                return true;
            }
            
            CloseDelete();
            for (var i = 0; i < ArrayDeleteUser.length; i++) {
                oTable.fnDeleteRow($('#table-users tbody tr[user-id="' + ArrayDeleteUser[i] + '"]').index());
            }
            var name_users = '';
            for(var i=0; i<result.length; i++) {
                name_users = name_users + result[i].user_firstname + " " + result[i].user_lastname + (i!==result.length-1?', ':'');
            }
            CallNoty('success', '<i class="fa fa-check"></i> Delete ' + name_users + ' success.');
            return true;
        };
        http.send(form);
    });
    //Delete user
    
    //Recycle user
    var CallBoxRecycle = function() {
        $('#box-recycle-user .box-content .content').html('');
        $('#box-recycle-user').removeClass('hidden').addClass('animated bounceIn');
        setTimeout(function(){
            $('#box-recycle-user').removeClass('animated bounceIn');
        }, 1000);
        return true;
    };
    
    var CloseRecycle = function() {
        $('#box-recycle-user').removeClass('hidden').addClass('animated bounceOut');
        setTimeout(function(){
            $('#box-recycle-user').removeClass('animated bounceOut').addClass('hidden');
            $('#box-recycle-user .box-content .content').html('');
        }, 1000);
    };
    
    var CallProgressLoadRecycle = function() {
        $('#box-recycle-user .box-content .progress').removeClass('hidden');
    };
    
    var CallCloseProgressLoadRecycle = function() {
        $('#box-recycle-user .box-content .progress').addClass('hidden');
    };
    
    $(document).on('click', '#recycle-users', function(event) {
        event.preventDefault();
        CallProgressLoadRecycle();
        CallBoxRecycle();
        var http = new XMLHttpRequest();
        http.open('POST', params.url + '/admin/users/get-users-deleted', true);
        http.onload = function(event) {
            CallCloseProgressLoadRecycle();
            var result = JSON.parse(this.responseText);
            
            if(result.length === 0) {
                $('#box-recycle-user .box-content .content').html('<div class="row"> <div class="col-sm-12 col-xs-12 margin-5 text-center"> <i>No users here .....</i> </div> <div class="col-sm-12 col-xs-12 margin-5 text-right"> <button id="cancel-recycle" type="button" class="btn btn-sm btn-danger"><i class="fa fa-power-off"></i>&nbsp;&nbsp;&nbsp;Cancel</button> </div> </div>');
                return true;
            }
            
            $('#box-recycle-user .box-content .content').html(tmpl('recycle', result));
            return true;
        };
        http.send();
    });
    
    $(document).on('click', '.restore-user', function(event) {
        event.preventDefault();
        var div     = $(this).parent('div');
        div.append('<i style="color:#009E01" class="fa fa-spinner fa-pulse"></i>');
        var user_id = $(this).attr('user-id');
        var http    = new XMLHttpRequest();
        var form    = new FormData();
        form.append('user_id', user_id);
        http.open('POST', params.url + '/admin/users/restore-user', true);
        http.onload = function (event) {
            //console.log(this.responseText);return true;
            var result = JSON.parse(this.responseText);
            if(result.error !== undefined) {
                CallNoty('warning', '<i class="fa fa-exclamation-triangle"></i> ' + result.messenger);
                return true;
            }
            
            var index = oTable.fnAddData([
                '<center> <div class="checkbox-inline"> <label class="text-center"> <input type="checkbox" class="check-user" user-id="' + result.user_id + '">&nbsp; <i class="fa fa-square-o small"></i> </label> </div> </center>',
                '<a href="" class="edit-user" user-id="' + result.user_id + '"> <i class="fa fa-fw fa-pencil-square-o"></i> <i>Edit</i> </a> &nbsp;&nbsp; <a href=""class="delete-user" user-id="' + result.user_id + '"> <i class="fa fa-fw fa-trash-o"></i> <i>Delete</i> </a>',
                result.user_firstname + ' ' + result.user_lastname,
                result.user_email,
                //result.user_status===1?'checked':'',
                '<input type="checkbox" class="toggle-user-status" user-id="' + result.user_id + '" ' + (result.user_status===1?'checked':'') + ' />',
                'Information Technology',
                FormatDateTime(result.created_at, 'H:i d/m/Y')
            ]);
            
            $('#table-users tbody tr:eq(' + index + ') .toggle-user-status').bootstrapToggle({
                on: 'Active',
                off: 'Inactive',
                onstyle: 'success',
                offstyle: 'danger',
                width: 90,
                size: 'mini'
            });
            $('#table-users tbody tr:eq(' + index + ')').attr('user-id', result.user_id);
            
            CallNoty('success', '<i class="fa fa-check"></i> Restore ' + result.user_firstname + ' ' + result.user_lastname + ' success.');
            
            div.addClass('animated bounceOutRight');
            setTimeout(function () {
                div.remove();
                if ($('#box-recycle-user').find('.restore-user').length === 0) {
                    CloseRecycle();
                }
            }, 500);
        };
        http.send(form);
    });
    
    $(document).on('keyup', '#search-in-recycle', function(event) {
        var text    = $(this).val();
        var finded  = 0;
        $('#box-recycle-user .box-content .content .container-recycle-user').filter(function(index){
            if($(this).html().replace(/(<([^>]+)>)/ig,"").search(text) !== -1) {
                $(this).removeClass('hidden');
                finded = 1;
            }
            else {
                $(this).addClass('hidden');
            }
        });
        
        if(finded === 0) {
            if($('#box-recycle-user .box-content .content .empty-search-recycle').length !== 0) {
                return true;
            }
            $('#box-recycle-user .box-content .content div:nth-child(2)').after('<div class="col-sm-12 col-xs-12 margin-5 empty-search-recycle"><i>Sorry. No users you want to find here ....</i></div>');
            return true;
        }
        $('#box-recycle-user .box-content .content .empty-search-recycle').remove();
        return true;
    });
    
    $(document).on('click', '#cancel-recycle', function(event) {
        CloseRecycle();
    });
    //Recycle user
    
    //Remove user
    $(document).on('click', '.delete-forever-user', function(event) {
        event.preventDefault();
        var div             = $(this).parents('div.container-recycle-user');
        var user_id         = $(this).attr('user-id');
        var user_fullname   = $(this).attr('user-fullname');
        noty({
            theme   : 'relax',
            text    : '(BETA) Do you want to remove user <b><i>' + user_fullname + '</i></b> from system ?',
            layout  : 'topCenter',
            buttons: [
                {addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {

                        // this = button element
                        // $noty = $noty element
                        $noty.close();
                        div.addClass('animated zoomOutUp');
                        setTimeout(function () {
                            div.remove();
                            if ($('#box-recycle-user').find('.restore-user').length === 0) {
                                CloseRecycle();
                            }
                        }, 1000);
                        //noty({text: 'You clicked "Ok" button', type: 'success'});
                    }
                },
                {addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                        $noty.close();
                        return true;
                        //noty({text: 'You clicked "Cancel" button', type: 'error'});
                    }
                }
            ]
        });
    });
    //Remove user
});