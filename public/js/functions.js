var SendRequest = function (url, params, callback) {
    $.ajax({
        url: url,
        type: 'POST',
        data: params
    }).done(function (data) {
        callback(data);
    });
};

var CheckAll = function (id_check_all, class_check)
{
    if(!document.getElementById(id_check_all).checked) {
         $('input.' + class_check).prop('checked', false);
        return true;
    }
    
    $('input.' + class_check).map(function() {
        if($(this).prop('checked') !== document.getElementById(id_check_all).checked) {
            $(this).prop('checked', document.getElementById(id_check_all).checked);
        }
        else {
            $(this).prop('checked', !document.getElementById(id_check_all).checked);
        }
    });
};

// Create Alert Follow Boostrap
var Nofitication = function (_class, content, close)
{
    var string = '<div class="alert alert-' + _class + ' alert-dismissible" role="alert">';
    if(close !== undefined || close === true) {
        string = string + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    }
    string = string + content;
    string = string + '</div>';
    return string;
};

// Dùng để lấy params từ query string chuyền vào src script
var parseQuery = function (queryString) {

    var query = queryString.replace(/^[^\?]+\??/, '');
    var Params = new Object();
    if (!query)
        return Params; // return empty object
    var Pairs = query.split(/[;&]/);
    for (var i = 0; i < Pairs.length; i++) {
        var KeyVal = Pairs[i].split('=');
        if (!KeyVal || KeyVal.length !== 2)
            continue;
        var key = unescape(KeyVal[0]);
        var val = unescape(KeyVal[1]);
        val = val.replace(/\+/g, ' ');
        Params[key] = val;
    }
    return Params;
};
// lấy params từ script
var parseQueryScript = function (attr_name, attr_value)
{
    var scripts = document.getElementsByTagName('script');
    var src     = '';
    for (var item in scripts)
    {
        if (scripts[item].getAttribute(attr_name) === attr_value)
        {
            src = scripts[item].getAttribute('src');
            break;
        }
    }
    var params = parseQuery(src);
    return params;
};
// lấy params từ chính chi tiết đường dẫn
var parseQuerySrc = function (src_string) {
    var scripts = document.getElementsByTagName('script');
    var length  = scripts.length;
    var src     = '';
    for(var i=0; i<length; i++) {
        if(scripts[i].hasAttribute('src')) {
            if(scripts[i].getAttribute('src').indexOf(src_string) !== -1) {
                src = scripts[i].getAttribute('src');
                break;
            }
        }
    }
    var params = parseQuery(src);
    return params;
};

// Replace array string
String.prototype.replaceArray = function (find, replace) {
    if(find.length !== replace.length) {
        return false;
    }
    
    var replaceString = this;
    
    for (var i = 0; i < find.length; i++) {
        replaceString = replaceString.replace(find[i], replace[i]);
    }
    return replaceString;
};

// Get first in array
Array.prototype.first = function() {
    return this[0];
};

//Format Datetime Sql 
var FormatDateTime = function(datetime, format) {
    var date = new Date(datetime);
    return format.replaceArray([
        'd',
        'm',
        'Y',
        'H',
        'i',
        's'
    ],[
        date.getDate()<10?'0'+date.getDate():date.getDate(),
        date.getMonth()+1<10?'0'+(date.getMonth()+1):date.getMonth()+1,
        date.getFullYear(),
        date.getHours()<10?'0'+date.getHours():date.getHours(),
        date.getMinutes()<10?'0'+date.getMinutes():date.getMinutes(),
        date.getSeconds()<10?'0'+date.getSeconds():date.getSeconds()
    ]);
};
// Kiểm tra xem có phần tử nào của objects empty không
var CheckObjectEmpty = function(object, ignore) {
    for(var item in object) {
        if(ignore !== undefined) {
            if(ignore.indexOf(item)) {
                continue;
            }
        }
        if(object[item] === '') {
            return false;
        }
    }
    return true;
};

//Call noty

var CallNoty = function(type, messenger, layout) {
    if(layout === undefined) {
        layout = "bottomRight";
    }

    var n = noty({
        layout: layout,
        theme: 'relax', // or 'relax'
        type: type,
        text: messenger,
        timeout: 3000,
        maxVisible: 3,
        killer: true,
        animation: {
            open: 'animated fadeIn', // Animate.css class names
            close: 'animated fadeOutDown', // Animate.css class names
            easing: 'swing', // easing
            speed: 500 // opening & closing animation speed
        }
    });
    
    return true;
};