$(document).ready(function () {
    //For Add Variable
    var StartAdd, EndAdd, AllDay;
    //For Add Variable
    
    //For Edit Variable
    var CalEventEdit;
    //For Edit Variable
    
    var calendar = $('#plan').fullCalendar({
        header: {
            left    : 'prev,next today',
            center  : 'title',
            right   : 'month,agendaWeek'
        },
        selectable      : true,
        selectHelper    : true,
        select: function (start, end, allDay) {
            $('#add-event-end').datepicker({
                dateFormat  : "dd/mm/yy",
                gotoCurrent : true
            }).datepicker("setDate", end._d);
            $('#modal-add-event').modal('show');
            StartAdd    = start;
            EndAdd      = end;
            AllDay      = allDay;
            return true;
        },
        editable    : true,
        eventLimit  : true, // allow "more" link when too many events
        eventClick  : function(calEvent, jsEvent, view) {
            console.log(calEvent);
            CalEventEdit = calEvent;
            $('#event-name').html(calEvent.title);
            $('#event-description').html(calEvent.description);
            $('#modal-detail-event').modal('show');
            return true;
        },
        eventRender : function (event, element, icon) {
            if (event.description !== "") {
                element.attr('title', event.description);
            }
        },
        events      : [
            {
                id          : 1,
                title       : "Làm Website Vitajeans",
                description : "Dead Line 15/10",
                start       : '2015-08-25',
                end         : '2015-08-28'
            },
            {
                id          : 13,
                title       : "Làm Website Khánh",
                description : "Dead Line 15/10",
                start       : "2015-08-22",
                end         : "2015-08-24",
                color       : "#5CB85C"
            },
            {
                id          : 14,
                title       : "Làm Website Hoàng",
                description : "Dead Line 15/10",
                start       : '2015-08-25',
                color       : "#D9534F"
            },
            {
                id          : 25,
                title       : "Làm Website Vũ",
                description : "Dead Line 15/10",
                start       : '2015-08-16',
                end         : '2015-08-21',
                color       : "#F0AD4E"
            },
            {
                id          : 30,
                title       : "Đánh League of Legends",
                description : "Dead Line 15/10",
                start       : '2015-08-01',
                end         : '2015-08-06'
            }
        ]
    });
    
    //For Add Event
    $('#modal-add-event').on('hidden.bs.modal', function(event) {
        $('#add-event-name').val('');
        $('#add-event-description').val('');
    });
    
    $('#btn-add-event').on('click', function(event) {
        //console.log(StartAdd);
        //console.log(EndAdd);
        
        $('#modal-add-event').modal('hide');
        if($('#add-event-name').val() === '') {
            return true;
        }
        var end = $('#add-event-end').datepicker().val().toString().split("/");
        
        calendar.fullCalendar('renderEvent', {
            title       : $('#add-event-name').val(),
            description : $('#add-event-description').val(),
            start       : StartAdd,
            end         : end[2] + "-" + end[1] + "-" + end[0],
            allDay      : AllDay
        }, true);
        return true;
    });
    //For Add Event
    
    //For Edit Event 
    //Global Variable CalEventEdit
    $('#modal-edit-event').on('hidden.bs.modal', function(event) {
        $('#edit-event-name').val('');
        $('#edit-event-description').val('');
    });
    
    //Flag Variable CheckEdit check modal edit event was loaded by button call edit event
    var CheckEdit = false;
    $('#btn-call-edit-event').on('click', function(event) {
        CheckEdit = true;
        $('#edit-event-name').val(CalEventEdit.title);
        $('#edit-event-description').val(CalEventEdit.description);
        $('#edit-event-end').datepicker({
            dateFormat: "dd/mm/yy",
            gotoCurrent: true
        }).datepicker("setDate", CalEventEdit.end!==null?CalEventEdit.end._d:CalEventEdit.start._d);
        $('#modal-detail-event').modal('hide');
        $('#modal-detail-event').on('hidden.bs.modal', function(event) {
            if(CheckEdit === true) {
                $('#modal-edit-event').modal('show');
                CheckEdit = false;
            }
        });
        return true;
    });
    
    $('#btn-edit-event').on('click', function(event) {
        var end = $('#edit-event-end').datepicker().val().toString().split("/");
        
        CalEventEdit.title          = $('#edit-event-name').val();
        CalEventEdit.description    = $('#edit-event-description').val();
        CalEventEdit.end            = end[2] + "-" + end[1] + "-" + end[0];
        
        calendar.fullCalendar('updateEvent', CalEventEdit);
        $('#modal-edit-event').modal('hide');
        return true;
    });
    //For Edit Event
    
    //For Delete Event 
    $('.btn-delete-event').on('click', function(event) {
        var modal = $(this).parents('div.modal');
        calendar.fullCalendar('removeEvents', function(ev) {
            return (ev._id === CalEventEdit._id);
        });
        modal.modal('hide');
    });
    //For Delete Event
});