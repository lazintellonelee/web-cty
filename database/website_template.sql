-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 29, 2015 at 07:54 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `website_template`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department`
--

CREATE TABLE IF NOT EXISTS `tbl_department` (
  `department_id` int(11) NOT NULL,
  `department_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_plans`
--

CREATE TABLE IF NOT EXISTS `tbl_plans` (
  `plan_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `plan_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plan_description` text COLLATE utf8_unicode_ci NOT NULL,
  `plan_date_start` date DEFAULT NULL,
  `plan_time_start` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `plan_date_end` date NOT NULL,
  `plan_time_end` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `plan_color` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `user_id` int(11) unsigned NOT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_role` enum('admin','user') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `user_department_id` int(10) unsigned NOT NULL,
  `user_avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_age` int(2) NOT NULL,
  `user_birthday` date DEFAULT NULL,
  `user_sex` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `user_phone` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `user_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_status` int(1) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `user_name`, `user_firstname`, `user_lastname`, `user_password`, `user_salt`, `user_email`, `user_role`, `user_department_id`, `user_avatar`, `user_age`, `user_birthday`, `user_sex`, `user_phone`, `user_address`, `user_status`, `deleted_at`, `updated_at`, `created_at`) VALUES
(1, 'lehuyvu', 'Lê Huy', 'Vũ', '3f5e4feff9089084b7855efef32acdf8', '53a3a4bd0df210c6ba094363bd57e5c1', 'huyvule92@gmail.com', 'admin', 0, 'lehuyvu.jpg', 23, '1992-09-07', 'Male', '0933484561', '126/5 Trần Bình Trọng, Phường 2, Quận 5, TP.Hồ Chí Minh', 1, NULL, '2015-08-25 10:25:01', '2015-08-22 04:02:43'),
(2, 'donguyenminhtrang', 'Đỗ Nguyễn Minh', 'Trang', '3f5e4feff9089084b7855efef32acdf8', '53a3a4bd0df210c6ba094363bd57e5c1', 'dnmt92@gmail.com', 'user', 0, 'donguyenminhtrang.png', 23, '1992-03-16', 'Female', '01267198196', '495 Lầu 1 Trần Hưng Đạo, Phường 14, Quận 5, TP.Hồ Chí Minh', 1, NULL, '2015-08-28 18:24:01', '2015-08-22 04:02:43'),
(3, 'trinhhuuvuong', 'Trịnh Hữu', 'Vương', '3f5e4feff9089084b7855efef32acdf8', '53a3a4bd0df210c6ba094363bd57e5c1', 'droptheworst@gmail.com', 'user', 0, 'vuong.png', 21, '1994-07-04', 'Male', '0972619470', 'Phạm Thế Hiển, Quận 8', 0, NULL, '2015-08-26 13:50:18', '2015-08-22 04:02:43'),
(4, 'nguyenthanhtuyen', 'Nguyễn Thanh', 'Tuyến', '3f5e4feff9089084b7855efef32acdf8', '53a3a4bd0df210c6ba094363bd57e5c1', 'the_death_dance92@gmail.com', 'user', 0, 'nguyenthanhtuyen.png', 23, '1992-12-09', 'Male', '01254516590', '45 Trần Nhân Trung ', 0, NULL, '2015-08-26 13:50:15', '2015-08-22 04:02:43'),
(5, 'nguyenphanloc', 'Nguyễn Phan', 'Lộc', '3f5e4feff9089084b7855efef32acdf8', '53a3a4bd0df210c6ba094363bd57e5c1', 'locnumber1@gmail.com', 'user', 0, 'nguyenphanloc.png', 23, '1992-04-12', 'Male', '', '64x Nguyễn Kiệm ', 1, NULL, '2015-08-27 12:37:56', '2015-08-22 04:02:43'),
(6, 'vuhoangphuongkhanh', 'Vũ Hoàng Phương', 'Khanh', '3f5e4feff9089084b7855efef32acdf8', '53a3a4bd0df210c6ba094363bd57e5c1', 'vuhoangphuongkhanh@gmail.com', 'user', 0, 'vuhoangphuongkhanh.jpg', 23, '1992-05-29', 'Male', '0908108452', 'Quận 11', 1, NULL, '2015-08-26 00:04:01', '2015-08-22 04:02:43'),
(7, 'trinhhuuhoang', 'Trịnh Hữu', 'Hoàng', 'b1a8a3911b60d078ca3220f3b9ae6a72', 'f3d3125585778ad415561ebed6ed687d', 'trinhhuuhoang@gmail.com', 'user', 0, '', 0, NULL, '', '', '', 0, NULL, '2015-08-28 21:36:06', '2015-08-28 14:36:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_department`
--
ALTER TABLE `tbl_department`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `tbl_plans`
--
ALTER TABLE `tbl_plans`
  ADD PRIMARY KEY (`plan_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_department`
--
ALTER TABLE `tbl_department`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_plans`
--
ALTER TABLE `tbl_plans`
  MODIFY `plan_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
